﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Anonymization
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnProcess_Click(object sender, RoutedEventArgs e)
        {

            string text = "7909106262";

            var encr = DecodeX509PublicKey(Encoding.ASCII.GetBytes(this.txtKey.Text));

            var res = encr.Encrypt(Encoding.ASCII.GetBytes("7909106262"), false);
            
            //encr.ImportParameters()
            return;
            if (this.txtInputFolder.Text == null || this.txtInputFolder.Text == string.Empty)
            {
                MessageBox.Show("Не сте избрали папка за входните файлове.");
                return;
            }

            if (this.txtOutputFolder.Text == null || this.txtOutputFolder.Text == string.Empty)
            {
                MessageBox.Show("Не сте избрали папка за изходните файлове.");
                return;
            }

            if (this.txtKey.Text == null || this.txtKey.Text == string.Empty)
            {
                MessageBox.Show("Не сте избрали ключ.");
                return;
            }

            //check if input folder exists
            if(Directory.Exists(this.txtInputFolder.Text) == false)
            {
                MessageBox.Show("Избраната входна папка не съществува или не е достъпна");
                return;
            }
            //check if files exist
            var files = Directory.GetFiles(this.txtInputFolder.Text, "*.xml|*.doc|*.docx|*.rtf");
            if(files.Count() == 0)
            {
                MessageBox.Show("Избраната входна папка е празна или не съдържа поддържани файлове");
                return;
            }
            //check if output folder exists
            if (Directory.Exists(this.txtOutputFolder.Text) == false)
            {
                MessageBox.Show("Избраната изходна папка не съществува или не е достъпна");
                return;
            }

            var outFiles = Directory.GetFiles(this.txtOutputFolder.Text);
            if(outFiles.Count() > 0)
            {
                if(MessageBox.Show("Избраната изходна папка вече съдържа файлове. Възможно е при съвпадение на имената някои от тях да бъдат унищожени! Желаете ли да продължите?", "Въпрос", MessageBoxButton.YesNo) == MessageBoxResult.No)
                {
                    return;
                }
            }
            //foreach input file - anonymise and produce output
            //check file type - if invalid
            //if GP
            //else Hosp
            //else Epicrisis - later... much later
        }

        private void BtnSelectKey_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog win = new OpenFileDialog();
            if(win.ShowDialog() == true)
            {
                string contents = File.ReadAllText(win.FileName);
                //check if file contains valid public key
                if(contents.StartsWith("-----BEGIN PUBLIC KEY-----") == false || contents.EndsWith("-----END PUBLIC KEY-----"))
                {
                    MessageBox.Show("Избрали сте невалиден файл с ключ");
                    return;
                }

                this.txtKey.Text = contents.Replace("-----BEGIN PUBLIC KEY-----\n", "").Replace("-----END PUBLIC KEY---- -", "").Trim();
            }
        }

        private void BtnSelectOutputFolder_Click(object sender, RoutedEventArgs e)
        {
            RadOpenFolderDialog win = new RadOpenFolderDialog();
            if (win.ShowDialog() == true)
            {
                if (Directory.Exists(win.FileName) == false)
                {
                    MessageBox.Show("Избраната изходна папка не съществува или не е достъпна");
                    return;
                }
                //check if files exist
                var files = Directory.GetFiles(win.FileName);
                if (files.Count() > 0)
                {
                    if (MessageBox.Show("Избраната изходна папка вече съдържа файлове. Възможно е при съвпадение на имената някои от тях да бъдат унищожени! Желаете ли да продължите?", "Въпрос", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    {
                        return;
                    }
                }
                this.txtOutputFolder.Text = win.FileName;
            }
            
        }

        private void BtnSelectInputFolder_Click(object sender, RoutedEventArgs e)
        {
            RadOpenFolderDialog win = new RadOpenFolderDialog();
            if (win.ShowDialog() == true)
            {
                if (Directory.Exists(win.FileName) == false)
                {
                    MessageBox.Show("Избраната входна папка не съществува или не е достъпна");
                    return;
                }
                //check if files exist
                var files = Directory.GetFiles(win.FileName, "*.xml"); //"*.xml|*.doc|*.docx|*.rtf"
                if (files.Count() == 0)
                {
                    MessageBox.Show("Избраната входна папка е празна или не съдържа поддържани файлове");
                    return;
                }

                this.txtInputFolder.Text = win.FileName;
            }
        }

        public string EncryptString(string inputString, int dwKeySize,
                             string xmlString)
        {
            // TODO: Add Proper Exception Handlers
            RSACryptoServiceProvider rsaCryptoServiceProvider =
                                          new RSACryptoServiceProvider(dwKeySize);
            rsaCryptoServiceProvider.FromXmlString(xmlString);
            int keySize = dwKeySize / 8;
            byte[] bytes = Encoding.UTF32.GetBytes(inputString);
            // The hash function in use by the .NET RSACryptoServiceProvider here 
            // is SHA1
            // int maxLength = ( keySize ) - 2 - 
            //              ( 2 * SHA1.Create().ComputeHash( rawBytes ).Length );
            int maxLength = keySize - 42;
            int dataLength = bytes.Length;
            int iterations = dataLength / maxLength;
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i <= iterations; i++)
            {
                byte[] tempBytes = new byte[
                        (dataLength - maxLength * i > maxLength) ? maxLength :
                                                      dataLength - maxLength * i];
                Buffer.BlockCopy(bytes, maxLength * i, tempBytes, 0,
                                  tempBytes.Length);
                byte[] encryptedBytes = rsaCryptoServiceProvider.Encrypt(tempBytes,
                                                                          true);
                // Be aware the RSACryptoServiceProvider reverses the order of 
                // encrypted bytes. It does this after encryption and before 
                // decryption. If you do not require compatibility with Microsoft 
                // Cryptographic API (CAPI) and/or other vendors. Comment out the 
                // next line and the corresponding one in the DecryptString function.
                Array.Reverse(encryptedBytes);
                // Why convert to base 64?
                // Because it is the largest power-of-two base printable using only 
                // ASCII characters
                stringBuilder.Append(Convert.ToBase64String(encryptedBytes));
            }
            return stringBuilder.ToString();
        }

        static string RSA(string input)
        {
            RSACryptoServiceProvider rsa = DecodeX509PublicKey(Convert.FromBase64String(GetKey()));

            return (Convert.ToBase64String(rsa.Encrypt(Encoding.ASCII.GetBytes(input), false)));
        }

        static string GetKey()
        {
            return File.ReadAllText("master.pub").Replace("-----BEGIN PUBLIC KEY-----", "").Replace("-----END PUBLIC KEY-----", "");
            //.Replace("\n", "");
        }

        private static bool CompareBytearrays(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;
            int i = 0;
            foreach (byte c in a)
            {
                if (c != b[i])
                    return false;
                i++;
            }
            return true;
        }

        public static RSACryptoServiceProvider DecodeX509PublicKey(byte[] x509key)
        {
            // encoded OID sequence for  PKCS #1 rsaEncryption szOID_RSA_RSA = "1.2.840.113549.1.1.1"
            byte[] SeqOID = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };
            byte[] seq = new byte[15];
            // ---------  Set up stream to read the asn.1 encoded SubjectPublicKeyInfo blob  ------
            MemoryStream mem = new MemoryStream(x509key);
            BinaryReader binr = new BinaryReader(mem);    //wrap Memory Stream with BinaryReader for easy reading
            byte bt = 0;
            ushort twobytes = 0;

            try
            {

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                seq = binr.ReadBytes(15);       //read the Sequence OID
                if (!CompareBytearrays(seq, SeqOID))    //make sure Sequence for OID is correct
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8103) //data read as little endian order (actual data order for Bit String is 03 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8203)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                bt = binr.ReadByte();
                if (bt != 0x00)     //expect null byte next
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                byte lowbyte = 0x00;
                byte highbyte = 0x00;

                if (twobytes == 0x8102) //data read as little endian order (actual data order for Integer is 02 81)
                    lowbyte = binr.ReadByte();  // read next bytes which is bytes in modulus
                else if (twobytes == 0x8202)
                {
                    highbyte = binr.ReadByte(); //advance 2 bytes
                    lowbyte = binr.ReadByte();
                }
                else
                    return null;
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };   //reverse byte order since asn.1 key uses big endian order
                int modsize = BitConverter.ToInt32(modint, 0);

                byte firstbyte = binr.ReadByte();
                binr.BaseStream.Seek(-1, SeekOrigin.Current);

                if (firstbyte == 0x00)
                {   //if first byte (highest order) of modulus is zero, don't include it
                    binr.ReadByte();    //skip this null byte
                    modsize -= 1;   //reduce modulus buffer size by 1
                }

                byte[] modulus = binr.ReadBytes(modsize);   //read the modulus bytes

                if (binr.ReadByte() != 0x02)            //expect an Integer for the exponent data
                    return null;
                int expbytes = (int)binr.ReadByte();        // should only need one byte for actual exponent data (for all useful values)
                byte[] exponent = binr.ReadBytes(expbytes);

                // ------- create RSACryptoServiceProvider instance and initialize with public key -----
                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSAParameters RSAKeyInfo = new RSAParameters();
                RSAKeyInfo.Modulus = modulus;
                RSAKeyInfo.Exponent = exponent;
                RSA.ImportParameters(RSAKeyInfo);
                return RSA;
            }
            catch (Exception)
            {
                return null;
            }

            finally { binr.Close(); }

        }
    }
}
